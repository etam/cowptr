# C++ Copy-on-Wrtite-Pointer (`CoWPtr`) done right!

## Warning!!!

This implementation requires [`atomic_shared_ptr`](http://en.cppreference.com/w/cpp/experimental/atomic_shared_ptr). Because it's not implemented yet in gcc's libstdc++ nor in clang's libc++, `std::shared_ptr` is used instead.

THEREFORE IT'S NOT THREAD-SAFE YET!!!

## The problem of trurly concurrent `CoWPtr`

Before we begin, one thing must be said (even if for some it might be obvious): Implementing `CoWPtr` is just writing a wrapper around shared smart pointer, that makes a new copy of the shared object before any modification.

There are 2 places of possible data races in `CowPtr`:

1. Two threads trying to write to the shared object.
2. One thread copying the smart pointer object, while other thread is writing to the shared object.

The first problem is solved quite trivially: every thread copies the object, modifies the copy and keeps it.

To solve the second problem we need to ensure that the thread copying the smart pointer and potentially reading the shared object, does not see the shared object while it's modified by the other thread.

Modifying the shared object, that's accessible for other threads and using mutexes, would be pretty hard to do correctly and without much contention. There's easier way that's not using mutexes at all!

## The solution

CoWPtr allows to modify the shared object by this method (might slightly change when `mSharedObj` becomes `atomic_shared_ptr`):

```C++
template <typename F>
void modify(F f)
{
    auto newSharedObj = std::make_shared<T>(*mSharedObj);   // copy shared object
    f(*newSharedObj);                                       // modify
    mSharedObj = newSharedObj;                              // store
}
```

This solution has following properties:

* No other thread can access the shared object while being modified.
* `mSharedObj` is updated atomically.
* If `f` throws an exception, the `newSharedObj` is discarded and `mSharedObj` is kept unmodified.
* No mutexes - only atomics.
