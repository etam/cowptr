//      Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "foo.hpp"

#include <iostream>


int Foo::sId = 0;
bool Foo::sVerbose = false;

Foo::Foo()
    : mI{0}
    , mId{sId++}
{
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
}

Foo::Foo(int i)
    : mI{i}
    , mId{sId++}
{
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
}

Foo::Foo(const Foo& other)
    : mI{other.mI}
    , mId{sId++}
{
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
}

Foo::Foo(Foo&& other)
    : mI{other.mI}
    , mId{sId++}
{
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
}

Foo& Foo::operator=(const Foo& other)
{
    mI = other.mI;
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
    return *this;
}

Foo& Foo::operator=(Foo&& other)
{
    mI = other.mI;
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
    return *this;
}

Foo::~Foo()
{
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
}

int Foo::i() const
{
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
    return mI;
}

void Foo::i(int i_)
{
    mI = i_;
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
}

int Foo::id() const
{
    if (sVerbose)
        std::cout << __PRETTY_FUNCTION__ << ' ' << mId << ' ' << mI << '\n';
    return mId;
}
