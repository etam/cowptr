//      Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <etam/cowptr.hpp>

#include <exception>
#include <memory>

#include "foo.hpp"


BOOST_AUTO_TEST_SUITE( cowptr )

class TestError
    : public std::exception
{};

BOOST_AUTO_TEST_CASE( sharing )
{
    const auto foo = etam::makeCoW<Foo>(5);
    const auto bar = foo;
    BOOST_CHECK_EQUAL( foo.get(), bar.get() );
    BOOST_CHECK_EQUAL( foo.useCount(), 2 );
}

BOOST_AUTO_TEST_CASE( CoW )
{
    auto foo = etam::makeCoW<Foo>(5);
    const auto bar = foo;
    const auto qux = bar;
    const int newI = 7;

    foo.modify([newI](Foo& foo) { foo.i(newI); });

    BOOST_CHECK_EQUAL( foo->i(), newI );
    BOOST_CHECK_NE( foo.get(), bar.get() );
    BOOST_CHECK( foo.unique() );
    BOOST_CHECK_EQUAL( bar.get(), qux.get() );
    BOOST_CHECK_EQUAL( bar.useCount(), 2 );
}

BOOST_AUTO_TEST_CASE( CoWWithReturn )
{
    auto foo = etam::makeCoW<Foo>(5);
    const auto bar = foo;
    const auto qux = bar;
    const int newI = 7;
    const int ret = 13;

    const auto gotRet = foo.modify([newI,ret](Foo& foo) {
        foo.i(newI);
        return ret;
    });

    BOOST_CHECK_EQUAL( foo->i(), newI );
    BOOST_CHECK_EQUAL( ret, gotRet );
    BOOST_CHECK_NE( foo.get(), bar.get() );
    BOOST_CHECK( foo.unique() );
    BOOST_CHECK_EQUAL( bar.get(), qux.get() );
    BOOST_CHECK_EQUAL( bar.useCount(), 2 );
}

BOOST_AUTO_TEST_CASE( throwInModify )
{
    const int i = 5;
    auto foo = etam::makeCoW<Foo>(i);
    const auto fooPtr = foo.get();

    BOOST_REQUIRE_THROW(
        foo.modify([](Foo& foo) {
            foo.i(13);
            throw TestError{};
        }),
        TestError
    );

    BOOST_CHECK_EQUAL( foo->i(), i );
    BOOST_CHECK_EQUAL( foo.get(), fooPtr );
}

BOOST_AUTO_TEST_CASE( throwInModifyWithReturn )
{
    const int i = 5;
    auto foo = etam::makeCoW<Foo>(i);
    const auto fooPtr = foo.get();

    BOOST_REQUIRE_THROW(
        foo.modify([](Foo& foo) {
            foo.i(13);
            throw TestError{};
            return 7;
        }),
        TestError
    );

    BOOST_CHECK_EQUAL( foo->i(), i );
    BOOST_CHECK_EQUAL( foo.get(), fooPtr );
}

BOOST_AUTO_TEST_SUITE_END() // cowptr
