//      Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef FOO_HPP
#define FOO_HPP

class Foo
{
private:
    int mI;
    int mId;
    static int sId;

public:
    static bool sVerbose;

    Foo();
    Foo(int i);
    Foo(const Foo& other);
    Foo(Foo&& other);
    Foo& operator=(const Foo& other);
    Foo& operator=(Foo&& other);
    ~Foo();
    int i() const;
    void i(int i_);
    int id() const;
};

#endif // FOO_HPP
