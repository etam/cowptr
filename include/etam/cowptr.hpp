//      Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef ETAM_COWPTR_HPP
#define ETAM_COWPTR_HPP

#include <memory>
#include <type_traits>
#include <utility>


namespace etam {


template <typename T>
class CoWPtr
{
private:
    std::shared_ptr<T> mSharedObj; // TODO: atomic_shared_ptr

public:
    CoWPtr() = default;

    CoWPtr(std::nullptr_t)
        : CoWPtr()
    {}

private:
    explicit
    CoWPtr(std::shared_ptr<T> sharedObj)
        : mSharedObj{std::move(sharedObj)}
    {}

    template<typename T_, typename... Args>
    friend
    CoWPtr<T_> makeCoW(Args&&... args);

public:
    CoWPtr(const CoWPtr&) = default;
    CoWPtr(CoWPtr&&) = default;
    CoWPtr& operator=(const CoWPtr&) = default;
    CoWPtr& operator=(CoWPtr&&) = default;

    explicit
    CoWPtr(T* objPtr)
        : mSharedObj{objPtr}
    {}

    // TODO: constructors with deleters and allocators

    void reset() noexcept
    {
        mSharedObj.reset();
    }

    void reset(T* objPtr)
    {
        mSharedObj.reset(objPtr);
    }

    // TODO: reset with deleters and allocators

    void swap(CoWPtr& other) noexcept
    {
        mSharedObj.swap(other.mSharedObj);
    }

    const T* get() const noexcept
    {
        return mSharedObj.get();
    }

    // no 'T* get()'

    const T& operator*() const noexcept
    {
        return *mSharedObj;
    }

    const T* operator->() const noexcept
    {
        return mSharedObj.get();
    }

    long useCount() const noexcept
    {
        return mSharedObj.use_count();
    }

    bool unique() const noexcept
    {
        return mSharedObj.unique();
    }

    explicit operator bool() const noexcept
    {
        return static_cast<bool>(mSharedObj);
    }

    template <typename F,
              typename = std::enable_if_t<std::is_void<std::result_of_t<F(T&)>>::value> >
    void modify(F f)
    {
        auto newSharedObj = std::make_shared<T>(*mSharedObj);
        f(*newSharedObj);
        mSharedObj = newSharedObj;
    }

    template <typename F,
              typename = std::enable_if_t<!std::is_void<std::result_of_t<F(T&)>>::value> >
    auto modify(F f)
    {
        auto newSharedObj = std::make_shared<T>(*mSharedObj);
        const auto result = f(*newSharedObj);
        mSharedObj = newSharedObj;
        return result;
    }
};


template<typename T, typename... Args>
CoWPtr<T> makeCoW(Args&&... args)
{
    return CoWPtr<T>{std::make_shared<T>(std::forward<Args>(args)...)};
}


} // namespace etam

#endif // ETAM_COWPTR_HPP
